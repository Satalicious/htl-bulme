#include <stdio.h>

int main() {
  char text[] = "Freedom";
  char *ptr = text;
  while (*ptr) {
    printf("%c (%p)\n", *ptr, ptr);
    ptr++;
  }
}
