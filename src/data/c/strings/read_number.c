#include <stdio.h>

int main() {
  char line[5];
  int number = 0;
  printf("enter an integer (0 <= number <= 9999): ");
  fgets(line, 5, stdin);
  printf("You entered the text `%s`.\n", line);
  sscanf(line, "%d", &number);
  printf("This text, represented as an integer is `%d`.\n", number);
}
