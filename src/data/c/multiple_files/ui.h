#ifndef UI_H
#define UI_H

#include <stddef.h>

size_t get_dimension(int argc, char** argv);
void print_array(int array[], size_t dimension);
void print_shared();

#endif
