#include <stdio.h>
#include <stdbool.h>

int main() {
  bool b1 = true, b2 = false;
  char c1 = 'x', c2 = 115;
  int i = -5;
  unsigned int u = 210;
  long int l = 12345689;
  float f = 1.23;
  double d = 1.2345;
  printf("b1=%d b2=%d\n", b1, b2);
  printf("c1=%c c2=%c c2=%d\n", c1, c2, c2);
  printf("i=%d u=%u l=%ld\n", i, u, l);
  printf("f=%f d=%lf\n", f, d);
  return 0;
}
