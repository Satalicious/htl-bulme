#include <stdio.h>
#include <ctype.h>

void upper(char* text) {
  while (*text) {  // string ends with the `\0` character
    *text = toupper(*text);  // return upper case character
    text++;
  }
}

int main() {
  char name[] = "Chris";
  upper(name);
  printf("%s\n", name);
  return 0;
}
