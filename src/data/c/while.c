#include <stdio.h>

int main() {
  int user_input = 3;  // skipping actual input code
  while (user_input > 0) {  // execute block while expression evaluates to `true`
    printf("%d, ", user_input);
    --user_input;  // avoid side effects in statement above
  }
  printf("FIRE!\n");
  return 0;
}
