#include <stdio.h>

int main() {
  int user_input = 3;  // skipping actual input code
  for (int i = 0; i < user_input; ++i) {
    printf("counting up: %d\n", i);
  }
  for (int j = user_input; j >= 0; --j) {
    printf("counting down: %d\n", j);
  }
  for (int j = 0; j < 10; j += user_input) {
    printf("counting with step != 1: %d\n", j);
  }
  return 0;
}
