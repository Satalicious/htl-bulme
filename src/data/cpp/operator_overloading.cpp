#include <iostream>
#include <vector>

using namespace std;

template <class Type>
vector<Type> operator+(const vector<Type>& v1, const vector<Type>& v2) {
  vector<Type> v;
  v.insert(end(v), begin(v1), end(v1));
  v.insert(end(v), begin(v2), end(v2));
  return v;
}

template <class Type>
ostream& operator<<(ostream& out, const vector<Type>& v) {
  out << '[';
  for (const auto& e: v) {
    out << e << ", ";
  }
  out << "\b\b]"; // two backspace characters to overwrite final ", "
  return out;
}

int main(void) {
  vector<int> v1{1, 2, 3, 4, 5, 6};
  vector<int> v2(5, 1); v2.push_back(4);  // {1, 1, 1, 1, 1, 4}
  cout << v1 << endl;
  cout << v2 << endl;
  vector<int> v3 = v1 + v2;
  cout << v3 << endl;
}
