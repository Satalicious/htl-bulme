

const on_toggle_solution = (event) => {
  const answer = event.currentTarget;
  const solution = answer.querySelector('.solution');
  if (solution.style.visibility == 'visible') {
    solution.style.visibility = 'hidden';
  } else {
    solution.style.visibility = 'visible';
  }
}

const on_toggle_yes_no_solution = (event) => {
  const question = event.currentTarget;
  question.classList.toggle('show_solution');
}

// button is direct child of question
// making the entire question clickable would break regular checkbox by
// showing solutions immediately, hence I use a button
const on_toggle_mc_solution = (event) => {
  const question = event.currentTarget.parentElement;
  question.classList.toggle('show_solution');
}

const cyrb53 = function(str, seed = 0) {
  let h1 = 0xdeadbeef ^ seed, h2 = 0x41c6ce57 ^ seed;
  for (let i = 0, ch; i < str.length; i++) {
    ch = str.charCodeAt(i);
    h1 = Math.imul(h1 ^ ch, 2654435761);
    h2 = Math.imul(h2 ^ ch, 1597334677);
  }
  h1 = Math.imul(h1 ^ (h1>>>16), 2246822507) ^ Math.imul(h2 ^ (h2>>>13), 3266489909);
  h2 = Math.imul(h2 ^ (h2>>>16), 2246822507) ^ Math.imul(h1 ^ (h1>>>13), 3266489909);
  return 4294967296 * (2097151 & h2) + (h1>>>0);
};

const generate_hash = () => {
  const hash_field = document.querySelector('.printedHash');
  hash_field.innerHTML = cyrb53((+new Date).toString(36)).toString(16);
}

window.addEventListener('load', (event) => {
  const answers = document.querySelectorAll('.answer');
  answers.forEach((elem, index, list) => {
    elem.addEventListener('click', on_toggle_solution);
  });
  const yes_no_questions = document.querySelectorAll('.yes_no_question');
  yes_no_questions.forEach((elem, index, list) => {
    elem.addEventListener('click', on_toggle_yes_no_solution);
  });
  const mc_questions = document.querySelectorAll(`.mc_question > input[type='button']`);
  mc_questions.forEach((elem, index, list) => {
    elem.addEventListener('click', on_toggle_mc_solution);
  });
  generate_hash();
});
