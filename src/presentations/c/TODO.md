Slide decks for

* first steps: text editor, main, data types, and operators + precedence (solve a concrete problem)
* functions, parameters, return; omitting return -> void
* arrays - storing multiple values + for loops for iteration
* working with files (fopen etc)
  - read, write, append
* debugging c programs (commandline debugger lldb / gdb + ide incl. config)
* data structures in C (linked list, doubly linked list, binary trees, ...)
* enum (and union) (maybe add to struct presentation)
* doxygen documentation and generation
* not C specific: running unit tests in version control (example
  gitlab / github)
* recursion
* function pointer

## variadic functions
  Variadic functions are functions which take a variable number of arguments. In C programming, a variadic function will contribute to the flexibility of the program that you are developing.

The declaration of a variadic function starts with the declaration of at least one named variable, and uses an ellipsis as the last parameter, e.g.

int printf(const char* format, ...);
https://en.wikipedia.org/wiki/Variadic_function#In_C 

---
  Flow control—flow control and loops
  Functions—user-defined, standard library, and recursion
  Arrays—fixed-size arrays, pointers, and variable-sized arrays
  Files—reading and writing files
  Console programming—conio
  Putting it together—writing a turn-based game
