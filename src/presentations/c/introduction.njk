---
title: Programming in C
subtitle: Introduction
layout: layouts/presentation.njk
---

<section>
  <h2>The C Programming Language</h2>

  <section>
  <h3>What is C?</h3>
  <ul>
    <li>General-purpose, procedural computer programming language</li>
    <li>C supports structured programming and recursion</li>
    <li>Static type system</li>
    <li>Creates highly efficient and performant machine code</li>
    <li class="fragment">Wikipedia: <a href="https://en.wikipedia.org/wiki/C_(programming_language)">C (programming language)</a></li>
    <li class="fragment"><a href="https://www.openhub.net/languages/compare?utf8=%E2%9C%93&measure=commits&language_name%5B%5D=c&language_name%5B%5D=cpp&language_name%5B%5D=golang&language_name%5B%5D=java&language_name%5B%5D=-1&language_name%5B%5D=rust&language_name%5B%5D=-1&commit=Update">Open Hub Comparison</a></li>
    <li class="fragment"><a href="https://www.tiobe.com/tiobe-index/">TIOBE Index</a></li>
  </ul>
  </section>

  <section>
    <h3>Application Areas</h3>
    <ul>
      <li>Operating systems (eg. Linux, Android)</li>
      <li>Implementation of programming languages (eg. cpython)</li>
      <li>High performance computations</li>
      <li>Application software (eg. GNOME Desktop, GIMP, ...)</li>
      <li>Embedded systems</li>
      <li>"Green Computing"</li>
    </ul>
  </section>

  <section>
    <h3>History</h3>
    <dl>
      <dt>1978</dt>
      <dd>Brian Kernighan and Dennis Ritchie (K&R) published<br />
        "<a href="https://archive.org/details/cprogramminglang00bria/mode/2up">The C Programming Language</a>"</dd>
      <dt>C99</dt>
      <dd>Still not supported by all common C compilers</dd>
      <dt>C11, C17</dt>
  </section>
</section>

<section>
  <h2>Structure of a C Program</h2>
  <section>
{% highlight "c" %}
{% include "source/c/hello.c" %}
{% endhighlight %}
  </section>

  <section>
    <h3>Preprocessor Directives</h3>
    <p>Allow, among others, to use functions like <code>printf</code> declared
      elsewhere</p>
{% highlight "c 0" %}
{% include "source/c/hello.c" %}
{% endhighlight %}
  </section>

  <section>
    <h3>Comments</h3>
{% highlight "c" %}
// single line comment
int x = 5;  // comments can be added at the end of a line

/*
 * multiline comment
 * ...
 */
{% endhighlight %}
    <div class="fragment">
      <p>Removed by the preprocessor</p>
{% highlight "c 2" %}
{% include "source/c/hello.c" %}
{% endhighlight %}
    </div>
  </section>

  <section>
    <h3>Main Function</h3>
    <p>Serves a special purpose in C programs: program execution begins with
      <code>main</code></p>
{% highlight "c 3-6" %}
{% include "source/c/hello.c" %}
{% endhighlight %}
  </section>

  <section>
    <h3>Return Value</h3>
    <p>Indicate success (<code>0</code>) or a failure code to the calling
      operating system shell</p>
{% highlight "c 5" %}
{% include "source/c/hello.c" %}
{% endhighlight %}
  </section>
</section>

{# TODO: extend this presentation to include
- instructions on basic commandline compilation (also mention replit.com)
- data types
- Variable, Assignment
- Comment
- printf with format specifiers
- ...

move the detailed compilation steps to a designated set of slides for
working with multiple files
 #}

<section>
  <h2>Compiling and Executing a C Program</h2>

  <section>
    <p>For creating executables, a compiler toolchain is needed.</p>
    <ul>
      <li><a href="http://gcc.gnu.org/">GCC</a> - GNU Compiler Collection</li>
      <li><a href="https://clang.llvm.org/">Clang</a> - C language family
        frontend for LLVM</li>
      <li>MSVC - Microsoft Visual C and C++ compiler</li>
    </ul>
  </section>

  <section>
    <p>
      Creating a C executable is a four step process.
    </p>
    <ol>
      <li>Preprocessor</li>
      <li>Compiler</li>
      <li>Assembler</li>
      <li>Linker</li>
    </ol>
  </section>

  <section>
    <h3>Example</h3>
      <p>Download <a href="/data/c/hello.c">hello.c</a></p>
      <p>By default, all three stages (preprocessor, compiler and linker) are done</p>
{% highlight "bash" %}
clang hello.c
./a.out
{% endhighlight %}
<div class="fragment">
<p>Provide a name for the created executable (instead of the default
  <code>a.out</code>
</p>
{% highlight "bash" %}
clang hello.c -o hello
./hello
{% endhighlight %}
</div>
  </section>

  <section>
    <h3>1. Preprocessor</h3>
    <ul>
      <li>Essentially a pure text processor</li>
      <li>Removes comments, perform includes etc.</li>
      <li>Usually never done separately</li>
    </ul>
    <div class="fragment">
      <p>Stop after preprocessor and send output to <code>stdout</code></p>
{% highlight "bash" %}
clang -E hello.c
{% endhighlight %}
    </div>
  </section>

  <section>
    <h3>2. Compiler</h3>
    <ul>
      <li>Translates high level code to a lower level (assembly language)</li>
      <li>Parses the source code</li>
      <li>Performs type checking</li>
      <li>Usually never done separately</li>
    </ul>
    <div class="fragment">
      <p>Creates assembler files (usually <code>*.s</code>)</p>
{% highlight "bash" %}
clang -S hello.c
{% endhighlight %}
    </div>
  </section>

  <section>
    <h3>3. Assembler</h3>
    <ul>
      <li>Translates assembly language to object code (machine language)</li>
      <li>Creates <code>*.o</code> files</li>
      <li><strong>Usually done separately</strong> when working with multiple input files
        <ul class="fragment">
          <li>Impossible to create executables without <code>main(.)</code></li>
          <li>Impossible to create executables with multiple <code>main(.)</code></li>
        </ul>
      </li>
    </ul>
    <div class="fragment">
{% highlight "bash" %}
clang -c hello.c
{% endhighlight %}
    </div>
  </section>

  <section>
    <h3>4. Linker</h3>
    <ul>
      <li>Combines object files into a unified executable program, resolving all symbols</li>
      <li><strong>Has to be run separately</strong> when working with multiple input files</li>
    </ul>
    <div class="fragment">
{% highlight "bash" %}
clang -o hello hello.o
{% endhighlight %}
    </div>
  </section>
</section>

{% include 'slides/closing.njk' %}
