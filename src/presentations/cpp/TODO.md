- fast track to c++
- using STL (using objects)

write presentations around solving a problem (eg. vector)
- OOP 1 - create classes, member functions and attributes, private members (encapsulation)
- OOP 2 - composition and aggregation
- OOP 3 - inheritance

GUI
- wxwidgets
- qt
- gtkmm
  https://gitlab.gnome.org/GNOME/gtkmm-documentation/-/tree/master/examples
  `g++ *.cc -o program `pkg-config gtkmm-4.0 --cflags --libs``
